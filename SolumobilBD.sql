drop database if exists solumobil;
create database if not exists solumobil;
use solumobil;

create table Estado(   /*UN SERVICIO SOLO PUEDE TENER UN ESTADO*/
id_estado_PK     			int not null auto_increment,
nombre_estado               varchar(50),
primary key (id_estado_PK)
);

create table Rol(              /*---Los dos tipo de tecnico----*//*MUCHOS usuarioS DE USUARIO PUEDEn tener  UN mismo ROL*/
id_rol_PK                 int not null auto_increment, /*---Al cliente no le pide el rol----*/
estado_rol	              bit not null,
nombre_rol                varchar(100) not null,
primary key (id_rol_PK)
);

create table Usuario(     /*un usuario de usuario solo puede hacerse una vez, ya sea por el cliente o por el cualquier otro rol*/
id_usuario_PK             int not null auto_increment,  
id_rol_FK                 int not null,
estado_usuario            bit not null,
correo        			  varchar(200) unique not null, -- Aqui esta el UNIQUE
clave	                  varchar(200) not null,
fecha_creacion            datetime default current_timestamp null,
keyreg                    varchar(150) null,
foreign key (id_rol_FK)   references    Rol     (id_rol_PK),
primary key (id_usuario_PK)   
);

create table Barrio_Cliente(   /*un cliente solo puede tener un barrio, o muchos cleintes pueden tener el mismo barrio*/
id_barrio_PK              int not null auto_increment,
estado_bc             	  bit not null,
nombre_barrio             varchar(300) unique not null,
primary key (id_barrio_PK)
);

create table Cliente(     /*Un cliente puede tener muchas revisiones de servicio, muchas citaciones, muchos servicios*/
id_cliente_PK             int not null auto_increment,
id_barrio_FK              int,    /*MUCHOS CLIENTES TIENEN UN BARRIO*/
id_usuario_FK             int unique not null,   -- UNIQUE AQUI TAMBIEN 
tipo_doc_cli              varchar(40) null,
nombre_cliente            varchar(100) not null,
apellido_cliente          varchar(100) not null,    
num_id_cli                varchar(50) unique null,
telefono                  varchar(15) unique null,       /*---Campo sin calculo alfanumerico----*/
direccion_residencia      varchar(130) null,
foto_cliente              text null,
primary key (id_cliente_PK),
foreign key (id_barrio_FK)   references    Barrio_Cliente     (id_barrio_PK),
foreign key (id_usuario_FK)   references    Usuario     (id_usuario_PK)
);

create table Tecnico(      
id_tecnico_PK             int not null auto_increment,
tipo_doc_tec              varchar(40) not null,
id_usuario_FK             int unique not null,   -- UNIQUE AQUI TAMBIEN
nombre_tecnico            varchar(100) not null,
apellido_tecnico          varchar(100) not null,     
telefono                  varchar(20) unique null,
num_id_tec         		  varchar(20) unique not null,
foto_tecnico              text null,
desc_tecnico              varchar(300) null,
direccion_residencia      varchar(130) null,
foreign key (id_usuario_FK)   references    Usuario     (id_usuario_PK),
primary key (id_tecnico_PK)
);


create table Marca_Artefacto(        
id_marca_artefacto_PK     int not null auto_increment,
estado_ma                 bit not null,
nombre_marca              varchar(150) unique not null,
primary key (id_marca_artefacto_PK)
);


create table Categoria_Artefacto(    
id_categoria_artefacto_PK   int not null auto_increment,
estado_ca            	    bit not null,
nombre_categoria_artefacto  varchar(100) unique not null,
primary key (id_categoria_artefacto_PK)
);

create table Artefacto(             
id_artefacto_PK           int not null auto_increment,
id_marca_artefacto_FK     int not null unique,
id_categoria_artefacto_FK int not null unique,
estado_artefacto          bit not null,
modelo                    varchar(250) unique not null,
caracteristicas           varchar(1000) null,
imei                      varchar(16) null,
serie                     varchar(15) null,
foto_artefacto            varchar(200) null,
foreign key (id_marca_artefacto_FK)     references  Marca_Artefacto      (id_marca_artefacto_PK),
foreign key (id_categoria_artefacto_FK)     references  Categoria_Artefacto      (id_categoria_artefacto_PK),
primary key (id_artefacto_PK)
);


create table Categoria_Servicio(        
id_categoria_servicio_PK     int not null auto_increment,
estado_cs                    bit not null,
nombre_cs    				 varchar(200) not null,
primary key (id_categoria_servicio_PK) 
);

/*
create table Citacion(     
id_citacion_PK            int not null auto_increment,
estado_citacion           bit not null,
direccion_lugar           varchar(300)  not null,
-- fecha_encuentro           date not null,
-- hora_encuentro            time null,
primary key (id_citacion_PK)
);
*/

create table Servicio(        
id_servicio_PK            int not null auto_increment,
id_cliente_FK		      int not null,
id_estado_servicio_FK     int not null,
fecha_peticion            date null,
hora_peticion             time null,
foreign key (id_estado_servicio_FK)       references  Estado      (id_estado_PK),
foreign key (id_cliente_FK)       references  Cliente    (id_cliente_PK),
primary key (id_servicio_PK)
);

-- DETALLES
create table Artefacto_Servicio(      
id_artefacto_servicio_PK   int not null auto_increment,
id_artefacto_FK 		  int not null,
id_servicio_FK 		      int not null,
foreign key (id_artefacto_FK)       references  Artefacto      (id_artefacto_PK),
foreign key (id_servicio_FK)       references  Servicio      (id_servicio_PK),
primary key (id_artefacto_servicio_PK)
);

/*
create table Citacion_Servicio(      
id_citacion_servicio_PK   int not null auto_increment,
id_servicio_FK 		      int not null,
id_citacion_FK 		      int not null,
foreign key (id_servicio_FK)       references  Servicio      (id_servicio_PK),
foreign key (id_citacion_FK)       references  Citacion              (id_citacion_PK),
primary key (id_citacion_servicio_PK)
);
*/

create table Categorias_Servicios( -- detalle cat ser
id_categorias_servicios_PK   int not null auto_increment,
id_servicio_FK 		      int not null,
id_categoria_servicio_FK 		      int not null,
foreign key (id_servicio_FK)       references  Servicio      (id_servicio_PK),
foreign key (id_categoria_servicio_FK)       references  Categoria_Servicio (id_categoria_servicio_PK),
primary key (id_categorias_servicios_PK)
);

create table Revision_Servicio(      
id_revision_servicio_PK   int not null auto_increment,
id_servicio_FK 		      int not null,
is_software               bit null,
is_hardware               bit null,
descripcion_cliente       varchar(700) null, 
diagnostico               varchar(650) not null,
costo_revision            float null,
costo_domicilio           float null,
costo_total               float not null,
fecha_inicio              datetime default null,
fecha_fin                 datetime default null,
fecha_encuentro           date null,
hora_encuentro            time null,             
direccion_lugar           varchar(300) null,
evidencia                 varchar(300) null,   
foreign key (id_servicio_FK)       references  Servicio      (id_servicio_PK),
primary key (id_revision_servicio_PK)
);

create table Tecnico_Servicio(      
id_tecnico_servicio_PK   int not null auto_increment,
id_servicio_FK 		      int not null,
id_tecnico_asignado_FK    int not null,
foreign key (id_servicio_FK)       references  Servicio      (id_servicio_PK),
foreign key (id_tecnico_asignado_FK)       references  Tecnico              (id_tecnico_PK),
primary key (id_tecnico_servicio_PK)
);

-- INSERCION DE DATOS
insert into Estado values
(null,"En proceso"),
(null,"En espera"),
(null,"Finalizado"),
(null,"Cancelado");
                          
insert into Rol values
(null,1,'Tecnico Administrador'),
(null,1,'Tecnico Empleado'),
(null,1,'Cliente'),
(null,0,'Cliente Externo'); 
/*
insert into Usuario values
(null,'1',1,'vargas.rodolfo0627@gmail.com','123',''),
(null,'1',1,'oscarjaviervargas@hotmail.com','delfinesvoladores2020',''),
(null,'2',1,'victor2014guerrero@gmail.com','124',''),
(null,'3',1,'hduque@gmail.com','132',''),
(null,'3',1,'na1@gmail.com','0000000',''),
(null,'3',1,'na2@gmail.com','0000000',''),
(null,'3',1,'na3@gmail.com','0000000',''),
(null,'3',1,'na4@gmail.com','0000000',''),
(null,'3',1,'na5@gmail.com','0000000',''); 
*/

insert into Barrio_Cliente values(null,1,'Villa Teresita - Engativa'),
									 (null,1,'Villa Gladys - Engativa'),
									 (null,1,'Linterama -Engativa');
                                     
/*                                     
insert into Cliente values
(null,'1','5','Cédula de Ciudadanía','Dario',
'Vargas','1121665778','3274368118','Crr 116 n 66- 87',''),
(null,'1','4','Cédula de Ciudadanía','Hernan',
'Duque','1221665778','3264368118','Crr 116 n 66- 87',''),
(null,'1','6','Cédula de Ciudadanía','Jose',
'Díaz','1321665778','3254368118','Crr 116 n 66- 87',''),
(null,'1','7','Cédula de Ciudadanía','Sandra','Guevara',
'1421665778','3244368118','Crr 116 n 66- 87',''),
(null,'1','8','Cédula de Ciudadanía','Alejandro','Suarez',
'1621665778','3234368118','Crr 116 n 66- 87',''),
(null,'1','9','Cédula de Ciudadanía','Aratza','Contreras',
'N/A','N/A','Crr 116 n 66- 87','');
*/
/*
insert into Tecnico values(null,'CC Cédula de Ciudadanía','1','Rodolfo','Vargas','3208557457','80115506','assets/img/technicians/papa.jpg','Técnico en reparación de dispositivos movíles y electricista','Carrera 116C #66-87'),
						  (null,'CE Cédula de Extranjería','3','Victor','Guerrero','3112710141','27168101','assets/img/technicians/vic.jpg','Técnico en reparación de dispositivos movíles','Villa Teresita'),
						  (null,'CC Cédula de Ciudadanía','2','Óscar Javier','Vargas Díaz','3133043714','1000620103','assets/img/technicians/os.jpg','Desarrollador de software','Carrera 116C #66-87'); 
*/
insert into Marca_Artefacto values(null,1,'Samsung'),
									 (null,1,'Huawei'),
									 (null,1,'Motorola'),
									 (null,1,'Asus'),
									 (null,1,'Lg'),
                                     (null,1,'Avvio'),
									 (null,1,'Blu'),
									 (null,1,'Xiaomi'),
									 (null,1,'Nokia'),
                                     (null,1,'Alcatel'),
                                     (null,1,'Iphone'),
                                     (null,1,'HTC'),
                                     (null,1,'Sony'),
                                     (null,1,'HP'),
                                     (null,1,'Dell'),
                                     (null,1,'Krono'),
                                     (null,1,'MyMmobile'),
                                     (null,1,'Zte'),
                                     (null,1,'apple'),
                                     (null,1,'lanix'),
                                     (null,1,'microsoft'),
                                     (null,1,'vodafone'),
                                     (null,1,'Azumi'),
                                     (null,1,'Bt-Speaker'),
                                     (null,1,'Philips'),
									 (null,1,'Lenovo');  
                                     
                                     
insert into Categoria_Artefacto values(null,1,'Celular'),
									 (null,1,'Tablet'),
									 (null,1,'Computador'),
									 (null,1,'Portatil'),
									 (null,1,'Reproductor de sonido'),
									 (null,1,'Otro');    
                                     
                                     
insert into Artefacto values
(null,'1','1',1,'Samsung Galaxy J5 Prime (SM-G570M)', 
'El Samsung Galaxy J5 Prime es una variante del Galaxy J5 con una pantalla HD de
 5 pulgadas, procesador quad-core a 1.4GHz, 2GB de RAM, 16GB de almacenamiento 
 interno expandible, cámara principal de 13 megapixels con flash LED, cámara 
 frontal de 5 MP, lector de huellas dactilares, chasis metálico y Android 6.0 ...',
 '1234567891234567','123456789123456',''),
(null,'2','2',1,'Tablet Krono 7031 Model Network', 
'Especificaciones de MODEL-7021.
PANTALLA --- LCD 7'' 1024X600 Multi Touch (Capacitive 5 puntos)
PROCESADOR --- CPU MTK 8312 a 1.3 GHz GPU Mall 400MP.
MEMORIA --- 8GB flash memory, 512MB RAM.
SISTEMA OPERATIVO --- Android Jelly Bean 4.2.2.
CONECTIVIDAD --- Wi-Fi IEEE 802.11b/g/n, Bluetooth 3.0/Conexion 2G.',
'1234567891234567','123456789123456',''),
(null,'3','3',0,'Tablet Krono 7032 Model Network', 
'Especificaciones de MODEL-7021.
PANTALLA --- LCD 7'' 1024X600 Multi Touch (Capacitive 5 puntos)
PROCESADOR --- CPU MTK 8312 a 1.3 GHz GPU Mall 400MP.
MEMORIA --- 8GB flash memory, 512MB RAM.
SISTEMA OPERATIVO --- Android Jelly Bean 4.2.2.
CONECTIVIDAD --- Wi-Fi IEEE 802.11b/g/n, Bluetooth 3.0/Conexion 2G.',
'1234567891234567','123456789123456','');  

							
insert into Categoria_Servicio values(null,1,'Cambio de pantalla'),
                                      (null,1,'Cambio de puerto de carga'),
                                      (null,1,'Cambio de puerto plu'),
                                      (null,1,'Cambio de tapa trasera'),
                                      (null,1,'Cambio de visor'),
                                      (null,1,'Quitar cuenta Google'),
                                      (null,1,'Hard reset (reseteo de fabrica)'),
                                      (null,1,'Cambio de sistema operativo (Flasheo)'),
                                      (null,1,'Creacion de cuentas en apps');                                     
                 
/*
insert into Citacion values(null,1,'cll 52 chapinero alto'),
                           (null,1,'cll 40 sur 23-34 suba'),
                           (null,1,'carrera 51 fontibon'); 
*/
/*
insert into Servicio values(null,'1','1','2020-01-01','12:12:12',50000),                           
                           (null,'2','2','2020-01-01','12:12:12',90000),
                           (null,'3','3','2020-01-01','12:12:12',10000),
                           (null,'4','3','2020-01-01','12:12:12',520000),
						   (null,'5','4','2020-01-01','12:12:12',40000);
                           
                         
insert into Revision_Servicio values
(null,'1',1,1,'Trabajo en construccion y se ensucio','Hay que cambiar el display',5000,null,null,'2020-04-01','13:30:00','Carrera 116C #66-87'),
(null,'2',0,0,'Se me cayó','Hay que cambiar el display',5000,null,null,'2020-04-01','13:30:00','Carrera 116C #66-87'),
(null,'3',1,1,'Se mojo','Hay que cambiar el display',5000,null,null,'2020-04-01','13:30:00','Carrera 116C #66-87'),
(null,'4',0,0,'Lo golpearon','Hay que cambiar el display',5000,null,null,'2020-04-01','13:30:00','Carrera 116C #66-87'),
(null,'5',1,1,'Le movieron algo allí','Hay que cambiar el puerto de carga',5000,null,null,'2020-04-01','13:30:00','Carrera 116C #66-87'); 
*/
/*INSERT servicio, estado_servicio, citación*/
/*Pensar en ponerle un estado al tecnico mas adelante*/
/*INCLUIR IF NOT EXIST, UNIQUE, ENCRIPTAR*/
/*ME FALTA INSERTAR DATOS*/


/**/

/*CONSULTAS MULTITABLA*/
/*select * from Servicio inner join Cliente on Servicio.id_cliente_FK = Cliente.id_cliente_PK; *//*SERVICIO Y CLIENTE*/

/*obtener datos*/
/*
CREATE VIEW datos_servicio_obt AS SELECT Ser.id_servicio_PK,Ser.id_citacion_FK,Ser.id_cliente_FK,
Ser.id_tecnico_asignado_FK,Ser.descripcion,Ser.fecha_peticion,Ser.hora_peticion,Ser.valor_precio_servicio,
esta.tipo,detalle.diagnostico, tec.nombre_tecnico, cli.nombre_cliente, cita.fecha_encuentro, cita.hora_encuentro
FROM Servicio Ser 
INNER JOIN Estado_Servicio esta ON Ser.id_estado_servicio_FK = esta.id_estado_servicio_PK
INNER JOIN Revision_Servicio detalle ON Ser.id_revision_servicio_FK = detalle.id_revision_servicio_PK
INNER JOIN Tecnico tec ON Ser.id_tecnico_asignado_FK = tec.id_tecnico_PK
INNER JOIN Cliente cli ON Ser.id_cliente_FK = cli.id_cliente_PK
INNER JOIN Citacion cita ON Ser.id_citacion_FK = cita.id_citacion_PK;*/


/*METER EN UNA VISTA PARA SIMPLIFICAR*/
/*CREATE VIEW datos_servicio AS SELECT Ser.id_servicio_PK,Ser.id_citacion_FK,Ser.id_cliente_FK,
Ser.id_tecnico_asignado_FK,Ser.id_revision_servicio_FK,Ser.id_estado_servicio_FK,Ser.descripcion,
Ser.fecha_hora_peticion,Ser.valor_precio_servicio,cli.id_barrio_FK,cli.id_usuario_FK, 
cli.id_tipo_documento_FK,cli.numero_documento,cli.telefono,cli.direccion_residencia,esta.tipo FROM Servicio
INNER JOIN Cliente ON Servicio.id_cliente_FK = Cliente.id_cliente_PK INNER JOIN Estado_Servicio 
ON Servicio.id_estado_servicio_FK = Estado_Servicio.id_estado_servicio_PK;*/

/*
CREATE VIEW SCCTRE_servicio AS SELECT  
									s.id_servicio_PK, -- servicio
                                     s.diagnostico,
									 -- date_format(s.fecha_hora_peticion,'%W, %D de %M,\n Hora: %h:%i%p') AS fecha_peticion,
									 s.fecha_peticion AS fp,
                                     s.hora_peticion,
									 s.valor_precio_servicio,	
									 s.id_citacion_FK,
                                     -- date_format(cita.fecha_hora_encuentro,'%W, %D de %M,\n Hora: %h:%i%p') AS fecha_cita, -- cita
									 cita.fecha_encuentro, -- citacion
                                     cita.hora_encuentro,
                                     cita.direccion_lugar,
									 s.id_tecnico_asignado_FK, -- tecnico
                                     t.nombre_tecnico,
                                     t.numero_documento,
                                     t.foto_tecnico,
                                     s.id_estado_FK, -- estado
                                     e.tipo,
                                     s.id_categoria_servicio_FK, -- categoria
									 cat.nombre_categoria
                                    -- SUM(valor_precio_servicio) AS ingresos, -- TCS_estadistica /*Estadisticas Iniciales*/
									-- ROUND(SUM(t.id_tecnico_PK/2 + c.id_cliente_PK/3),0) AS num_usuarios,
                                    -- ROUND(SUM(s.id_servicio_PK-s.id_servicio_PK*60/100),0) AS num_venta_total,
                                    -- ROUND(SUM(s.id_servicio_PK-s.id_servicio_PK*60/100),0) AS num_venta_semana,
                                    -- FLOOR(RAND()*(11-1)+1) AS num_random		*/
-- ------------------------------------------------------------------------------------------
/*
-- Consulta MEGAMultitabla - accediendo a 3 dimensiones
SELECT ma.nombre as marca, a.modelo,rs.diagnostico,rs.fecha_inicio,rs.fecha_fin,s.descripcion as servicio  from revision_servicio as rs 
		       inner join servicio as s on s.id_servicio_PK = rs.id_servicio_FK
               inner join artefacto as a on a.id_artefacto_PK = rs.id_artefacto_FK
               inner join marca_artefacto as ma on ma.id_marca_artefacto_PK = ma.id_marca_artefacto_PK;
*/				
-- ------------------------------------------------------------------------------------------    
-- VW Detail 6 (no se necesita)
/*CREATE VIEW VW_ASCCT_detail AS SELECT r.*,a.*,s.*,c.*,ca.*,t.*
FROM Revision_Servicio r
INNER JOIN Artefacto a ON r.id_artefacto_FK = a.id_artefacto_PK
INNER JOIN Servicio s ON r.id_servicio_FK = s.id_servicio_PK
INNER JOIN Citacion c ON r.id_citacion_FK = c.id_citacion_PK
INNER JOIN Categoria_Servicio ca ON r.id_categoria_servicio_FK = ca.id_categoria_servicio_PK
INNER JOIN Tecnico t ON r.id_tecnico_asignado_FK = t.id_tecnico_PK;

/*
CREATE PROCEDURE sp_getDetailById(IN id_servicio_PK int) 
SELECT *, a.modelo as nfacto, s.id_servicio_PK,c.id_citacion_PK  -- voy aqui	 
FROM Revision_Servicio rs
INNER JOIN Artefacto a 				ON a.id_artefacto_PK = rs.id_artefacto_FK
INNER JOIN Servicio s 				ON s.id_servicio_PK = rs.id_servicio_FK
INNER JOIN Citacion c 				ON c.id_citacion_PK = rs.id_citacion_FK
INNER JOIN Categoria_Servicio cs 	ON cs.id_categoria_servicio_PK = rs.id_categoria_servicio_FK
INNER JOIN Categoria_Artefacto ca 	ON ca.id_categoria_artefacto_PK = ca.id_categoria_artefacto_PK -- added
INNER JOIN Marca_Artefacto ma 	    ON ma.id_marca_artefacto_PK = ma.id_marca_artefacto_PK         -- added
INNER JOIN Tecnico t 				ON t.id_tecnico_PK = rs.id_tecnico_asignado_FK 
WHERE rs.id_servicio_FK = id_servicio_PK; -- ese es el berraco parametro

CALL sp_getDetailById(2);*/
-- ----------------------------------------------------------------------------------------        


SET lc_time_names = 'es_ES';

-- VISTA SERVICIO 1
CREATE VIEW SEC_servicio AS SELECT s.*,c.*,e.* 
FROM Servicio s 
INNER JOIN Estado e ON s.id_estado_servicio_FK = e.id_estado_PK
INNER JOIN Cliente c ON s.id_cliente_FK = c.id_cliente_PK;

-- VISTA CLIENTE 2
CREATE VIEW VW_cliente AS SELECT b.*,u.*,c.* -- crear una sola tabl
FROM Cliente c 
LEFT JOIN Barrio_Cliente b ON c.id_barrio_FK  = b.id_barrio_PK -- solo trae null sino encuentra coincidencias
INNER JOIN Usuario u        ON c.id_usuario_FK = u.id_usuario_PK; -- intersección con solo los datos que coinciden

-- VISTA TÉCNICO 3
CREATE VIEW VW_UTD_tecnico AS SELECT u.*,t.*
FROM Tecnico t 
INNER JOIN Usuario u ON t.id_usuario_FK = u.id_usuario_PK;

select * from VW_UTD_tecnico;

-- VISTA USUARIOS 4
CREATE VIEW VW_R_usuario AS SELECT u.*,r.*
FROM Usuario u 
INNER JOIN Rol r ON u.id_rol_FK = r.id_rol_PK;

-- VISTA ARTEFACTO 5
CREATE VIEW VW_MCE_artefacto AS SELECT a.*,m.*,ca.*
FROM Artefacto a
INNER JOIN Marca_Artefacto m ON a.id_marca_artefacto_FK = m.id_marca_artefacto_PK
INNER JOIN Categoria_Artefacto ca ON a.id_categoria_artefacto_FK = ca.id_categoria_artefacto_PK;

-- VISTA Categoria_Servicio 6
CREATE VIEW VW_cat_ser AS SELECT * FROM Categoria_Servicio;

-- VW BARRIO_CLIENTE
CREATE VIEW VW_barrio AS SELECT * FROM barrio_cliente; 

-- VW Roles
CREATE VIEW VW_role AS SELECT * FROM rol; 

-- VW VALIDAR USUARIO
/*
CREATE VIEW VW_validate_user AS SELECT u.*,t.*,c.*, r.nombre_rol as rol 
						FROM tecnico t
                        INNER JOIN Usuario u ON u.id_usuario_PK = t.id_usuario_FK
                        INNER JOIN Cliente c ON c.id_rol_PK = c.id_rol_PK
                        INNER JOIN Rol r ON r.id_rol_PK = t.id_rol_FK;
*/
-- START DETAILS WITH PARAMETER------------------------------------------------------

-- SP Detail artifact -- (1)
CREATE PROCEDURE sp_getArtServiceById(IN id_servicio_PK int) 
SELECT *, s.id_servicio_PK, ars.id_servicio_FK, a.id_artefacto_PK	 
FROM Artefacto_Servicio ars
INNER JOIN Artefacto a 				ON a.id_artefacto_PK = ars.id_artefacto_FK
INNER JOIN Marca_Artefacto ma 		ON ma.id_marca_artefacto_PK = ma.id_marca_artefacto_PK
INNER JOIN Categoria_Artefacto ca 	ON ca.id_categoria_artefacto_PK = ca.id_categoria_artefacto_PK
INNER JOIN Servicio s 				ON s.id_servicio_PK = ars.id_servicio_FK
WHERE ars.id_servicio_FK = id_servicio_PK; -- ese es el berraco parametro

CALL sp_getArtServiceById(2);
-- --------------------------- VMO AQUI
create procedure sp_getArtServiceClientById
(IN idSer int, IN idCli int) -- revision
select r.*,sec.* from revision_servicio r
inner join SEC_servicio sec on sec.id_servicio_PK = r.id_servicio_FK
where r.id_revision_servicio_PK = idSer and sec.id_cliente_FK = idCli;

-- SP Detail citaction  -- (2)
/*CREATE PROCEDURE sp_getCitServiceById(IN id_servicio_PK int) 
SELECT *, s.id_servicio_PK, cs.id_servicio_FK, c.id_citacion_PK
FROM Citacion_Servicio cs
INNER JOIN Servicio s 				ON s.id_servicio_PK = cs.id_servicio_FK
INNER JOIN Citacion c 				ON c.id_citacion_PK = cs.id_citacion_FK
WHERE cs.id_servicio_FK = id_servicio_PK; -- ese es el berraco parametro*/

-- CALL sp_getCitServiceById(2);

-- SP Details category -- (3)
CREATE PROCEDURE sp_getCatServiceById(IN id_servicio_PK int) 
SELECT *, s.id_servicio_PK, csss.id_servicio_FK, cas.id_categoria_servicio_PK
FROM Categorias_Servicios csss
INNER JOIN Servicio s 				ON s.id_servicio_PK = csss.id_servicio_FK
INNER JOIN Categoria_Servicio cas 	ON cas.id_categoria_servicio_PK = csss.id_categoria_servicio_FK
WHERE csss.id_servicio_FK = id_servicio_PK; -- ese es el berraco parametro

CALL sp_getCatServiceById(2);

-- SP Detail technical  -- (4)
CREATE PROCEDURE sp_getTechServiceById(IN id_servicio_PK int) 
SELECT *
FROM Tecnico_Servicio ts
INNER JOIN Servicio s 				ON s.id_servicio_PK = ts.id_servicio_FK
INNER JOIN Tecnico t 				ON t.id_tecnico_PK = ts.id_tecnico_asignado_FK
INNER JOIN Usuario u 				ON u.id_usuario_PK = t.id_usuario_FK 
INNER JOIN Rol r 					ON r.id_rol_PK = u.id_rol_FK  
WHERE ts.id_servicio_FK = id_servicio_PK; -- ese es el berraco parametro

CALL sp_getTechServiceById(2);

-- SP Details -- (5)
CREATE PROCEDURE sp_getDetServiceById(IN id_servicio_PK int) 
SELECT *,s.id_servicio_PK, rs.id_servicio_FK
FROM Revision_Servicio rs
INNER JOIN Servicio s ON s.id_servicio_PK = rs.id_servicio_FK
WHERE rs.id_servicio_FK = id_servicio_PK; 

CALL sp_getDetServiceById(1);

CREATE PROCEDURE sp_getDetServiceClientById
(IN idser int,IN idcli int) 
SELECT rs.*,sec.*
FROM Revision_Servicio rs
INNER JOIN SEC_servicio sec ON sec.id_servicio_PK = rs.id_servicio_FK
where sec.id_cliente_FK = idcli and rs.id_servicio_FK = idser; 

CALL sp_getDetServiceClientById(2,2);

create procedure SP_client_service
(IN id int) -- revision
select r.*,sec.* from revision_servicio r
inner join SEC_servicio sec on sec.id_servicio_PK = r.id_servicio_FK
where sec.id_cliente_FK = id and sec.id_estado_servicio_FK != 4 
order by sec.id_servicio_PK desc;

-- Traer todo para el usaurio(cliente o lo que sea individual) en sesion
create view VW_tecnico_servicio as select ts.*,t.* from tecnico_servicio ts
inner join tecnico t on t.id_tecnico_PK = ts.id_tecnico_asignado_FK;

create view VW_categorias_servicios as select css.*,cs.* from categorias_servicios css
inner join categoria_servicio cs on cs.id_categoria_servicio_PK = css.id_categoria_servicio_FK;

create view VW_artefacto_servicio as select ars.*,a.* from artefacto_servicio ars
inner join artefacto a on a.id_artefacto_PK = ars.id_artefacto_FK;
/*
create procedure SP_client_service
(IN id int) -- revision
select r.*,sec.* from revision_servicio r
inner join SEC_servicio sec on sec.id_servicio_PK = r.id_servicio_FK
where sec.id_cliente_FK = id order by r.id_revision_servicio_PK ASC;

call SP_client_service(4);*/

create procedure SP_client_serviceById
(IN id int, IN idCli int) -- revision
select r.*,sec.* from revision_servicio r
inner join SEC_servicio sec on sec.id_servicio_PK = r.id_servicio_FK
where r.id_revision_servicio_PK = id and sec.id_cliente_FK = idCli;

call SP_client_serviceById(8,4);

create procedure SP_client_tech
(IN id int)
select ta.*,t.* from tecnico_servicio ta
inner join Servicio s on s.id_servicio_PK = ta.id_servicio_FK
inner join Tecnico t  on t.id_tecnico_PK  = ta.id_tecnico_asignado_FK
where s.id_cliente_FK = id;

call SP_client_tech(4);
select * from cliente;





-- -----------------END DETAILS WITH PARAMETER------

-- START DETAILS------------------------------------------------------

-- SP Detail artifact -- (1)
CREATE PROCEDURE sp_getArtService() 
SELECT *, s.id_servicio_PK, ars.id_servicio_FK, a.id_artefacto_PK	 
FROM Artefacto_Servicio ars
INNER JOIN Artefacto a 				ON a.id_artefacto_PK = ars.id_artefacto_FK
INNER JOIN Marca_Artefacto ma 		ON ma.id_marca_artefacto_PK = ma.id_marca_artefacto_PK
INNER JOIN Categoria_Artefacto ca 	ON ca.id_categoria_artefacto_PK = ca.id_categoria_artefacto_PK
INNER JOIN Servicio s 				ON s.id_servicio_PK = ars.id_servicio_FK; 

CALL sp_getArtService();

-- SP Detail citaction  -- (2)
/*CREATE PROCEDURE sp_getCitService() 
SELECT *
FROM Citacion_Servicio cs
INNER JOIN Servicio s 				ON s.id_servicio_PK = cs.id_servicio_FK
INNER JOIN Citacion c 				ON c.id_citacion_PK = cs.id_citacion_FK; 

CALL sp_getCitService();
*/

-- SP Details catgeory -- (3)
CREATE PROCEDURE sp_getCatService() 
SELECT *
FROM Categorias_Servicios csss
INNER JOIN Servicio s 				ON s.id_servicio_PK = csss.id_servicio_FK
INNER JOIN Categoria_Servicio cas 	ON cas.id_categoria_servicio_PK = csss.id_categoria_servicio_FK; 

CALL sp_getCatService();


-- SP Detail technical  -- (4)
CREATE PROCEDURE sp_getTechService() 
SELECT *
FROM Tecnico_Servicio ts
INNER JOIN Servicio s 				ON s.id_servicio_PK = ts.id_servicio_FK
INNER JOIN Tecnico t 				ON t.id_tecnico_PK = ts.id_tecnico_asignado_FK
INNER JOIN Usuario u 				ON u.id_usuario_PK = t.id_usuario_FK 
INNER JOIN Rol r 					ON r.id_rol_PK = u.id_rol_FK; 

CALL sp_getTechService();

-- SP Details -- (5)
CREATE PROCEDURE sp_getDetService() 
SELECT *
FROM Revision_Servicio rs
INNER JOIN Servicio s 				ON s.id_servicio_PK = rs.id_servicio_FK; 

CALL sp_getDetService();

-- -----------------END DETAILS------


-- SP n° max de revisiones de un servicio (2)
/*CREATE PROCEDURE sp_getDetailsCount(IN id_servicio int)
SELECT *, MAX(rs.id_revision_servicio_PK) as maxRs
FROM Servicio s
INNER JOIN Revision_Servicio rs ON rs.id_revision_servicio_PK = rs.id_revision_servicio_PK
WHERE id_servicio_PK = id_servicio;

call sp_getDetailsCount(1);

-- SP conteo citaciones de una revisión (3)
CREATE PROCEDURE sp_getCitationsCount(IN id_revision int)
SELECT *, COUNT(c.id_citacion_PK) as maxCitaciones
FROM Revision_Servicio rs
INNER JOIN Citacion c ON rs.id_citacion_FK = c.id_citacion_PK
WHERE id_revision_servicio_PK = id_revision;

call sp_getCitationsCount(1);



/*TRIGGER QUE GUARDA LOS DATOS DEL ROL AL ACTUALIZAR*/
CREATE TABLE rol_actualizado
(
anterior_id_rol_PK INTEGER,
nuevo_id_rol_PK INTEGER, 
anterior_estado_rol BIT,
nuevo_estado_rol BIT,
anterior_nombre_rol VARCHAR(100),
nuevo_nombre_rol VARCHAR(100),
fecha TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER TR_rol_BU BEFORE UPDATE ON rol FOR EACH ROW INSERT INTO rol_actualizado
(anterior_id_rol_PK, nuevo_id_rol_PK, 
anterior_estado_rol, nuevo_estado_rol, 
anterior_nombre_rol, nuevo_nombre_rol,
fecha) 
VALUES
(
OLD.id_rol_PK, NEW.id_rol_PK, 
OLD.estado_rol, NEW.estado_rol, 
OLD.nombre_rol, NEW.nombre_rol, NOW());

/*TRIGGER QUE GUARDA LOS DATOS DEL SERVICIO AL ACTUALIZAR*/
CREATE TABLE servicio_actualizado
(
anterior_id_servicio_PK INTEGER,
nuevo_id_servicio_PK INTEGER, 
anterior_id_estado_servicio_FK INTEGER,
nuevo_id_estado_servicio_FK INTEGER,
anterior_fecha_peticion DATE,
nuevo_fecha_peticion DATE,
anterior_hora_peticion TIME,
nuevo_hora_peticion TIME,
anterior_costo_servicio FLOAT,
nuevo_costo_servicio FLOAT,
id_cliente_FK INT,
fecha_actualizacion TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER TR_servicio_BU BEFORE UPDATE ON servicio FOR EACH ROW INSERT INTO servicio_actualizado
(
anterior_id_servicio_PK, nuevo_id_servicio_PK,
anterior_id_estado_servicio_FK, nuevo_id_estado_servicio_FK,
anterior_fecha_peticion, nuevo_fecha_peticion,
anterior_hora_peticion, nuevo_hora_peticion,
id_cliente_FK, 
fecha_actualizacion
)
VALUES
(
OLD.id_servicio_PK, NEW.id_servicio_PK,
OLD.id_estado_servicio_FK, NEW.id_estado_servicio_FK,
OLD.fecha_peticion, NEW.fecha_peticion,
OLD.hora_peticion, NEW.hora_peticion,
id_cliente_FK, NOW()
);


-- UPDATE rol SET nombre_rol = 'Cliente Suspendido' WHERE id_rol_PK = 4;
-- SELECT * FROM rol_actualizado;
/*TRIGGER QUE GUARDA LOS DATOS DEL ROL AL ACTUALIZAR*/
/*CREATE TABLE servicio_actualizado
(
anterior_id_servicio_PK INTEGER,
nuevo_id_servicio_PK INTEGER, 
anterior_id_estado_servicio_FK INT,
nuevo_id_estado_servicio_FK INT,
anterior_nombre_rol VARCHAR(100),
nuevo_nombre_rol VARCHAR(100),
fecha TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TRIGGER TR_rol_BU BEFORE UPDATE ON rol FOR EACH ROW INSERT INTO rol_actualizado
(anterior_id_rol_PK, nuevo_id_rol_PK, 
anterior_estado_rol, nuevo_estado_rol, 
anterior_nombre_rol, nuevo_nombre_rol,
fecha) 
VALUES
(
OLD.id_rol_PK, NEW.id_rol_PK, 
OLD.estado_rol, NEW.estado_rol, 
OLD.nombre_rol, NEW.nombre_rol, NOW());
*/

-- REPORTES
create view VW_EP as select count(id_servicio_PK) as en_proceso from servicio where id_estado_servicio_FK = 1;
create view VW_EE as select count(id_servicio_PK) as en_espera from servicio where id_estado_servicio_FK = 2;
create view VW_EF as select count(id_servicio_PK) as finalizado from servicio where id_estado_servicio_FK = 3;
create view VW_EC as select count(id_servicio_PK) as cancelado from servicio where id_estado_servicio_FK = 4;

-- SP Promedio de ventas servicios totales (4)
/*CREATE PROCEDURE sp_getSalesService()
SELECT *, AVG(costo_servicio) as media
FROM Servicio; 
*/
-- descuento a los clientes frecuentes. mas de un millon de pesos. mas de 5 servicios							
-- eso del if en que casos se usa teniendo en cuenta que el lenguaje sea PHP no lo pueda hacer
-- si el cliente es frecuente que le haga un descuento
-- si hay pocas unidades en stock si profe depende el capital de la empresa
-- Hacer procedimientos para el SI combinados con vistas







