
use neptunoscar;

-- TALLER VISTAS
/*1. Cree una vista con el nombre del producto y la cantidad total vendida del mismo. */
-- tabla detalles_de_pedido y producto son las que trabajamos

CREATE VIEW PDP_cantidades_vendidas AS -- nombre de la vista
SELECT NombreProducto,SUM(Cantidad) AS Cantidad FROM producto p 
INNER JOIN detalles_de_pedido dp ON dp.idProducto = p.idProducto
GROUP BY NombreProducto; -- SE AGRUPA SEGUN LOS PRODUCTOS PARA QUE NO LOS REPITA                        
-- NO IMPORTAN LA MAYUS EN EL DELFIN PERO EN EL ELEFANTE SI
SELECT * FROM PDP_cantidades_vendidas WHERE NombreProducto LIKE '%W%'; -- primera letra en a
SELECT * FROM PDP_cantidades_vendidas WHERE NombreProducto LIKE '%R'; -- ultima letra en r


/*2. Cree una vista con el nombre del empleado, la edad y el total recaudado en ventas por
cada uno. */
-- TIMESTAMPDIFF RECIBE 3 PARAMETROS TAMBIEN FUNCIONAN LOS MESES, DIAS, HORAS, SEGUNDOS
-- se compara la fecha actual con la fecha en que nacio el empleado
-- NOW() SIRVE PARA DESPACHAR LA FECHA ACTUAL
CREATE VIEW EV_empleVent(nomEmpleado,apeEmpleado,edad,recaudo)AS
SELECT Nombre,Apellidos,TIMESTAMPDIFF(YEAR,fechaNacimiento,NOW()),
SUM(dept.preciounidad*dept.cantidad)-
(SUM(dept.preciounidad*dept.cantidad)*dept.descuento)-- no reconoce el descuento
from Empleado e
	INNER JOIN Pedido AS ped ON e.idEmpleado = ped.idEmpleado 
	INNER JOIN detalles_de_pedido AS dept ON ped.idPedido=dept.idPedido 
group by Nombre,Apellidos;

SELECT * FROM EV_empleVent ORDER BY recaudo desc; -- organización con order by = asc o desc
SELECT * FROM EV_empleVent ORDER BY edad asc;

-- GROUP BY Nombre; 
-- As Total_Vendido 
-- uso del objeto
select * from EV_empleVent;

/*3. Cree una vista con el nombre de la categoría, el nombre de los productos de cada
categoría y los proveedores de cada producto incluya solo las filas de los productos
empacados en botellas (bot). */
CREATE VIEW CPP_categoria_producto 
AS SELECT NombreCategoria,NombreProducto,NombreEmpresa FROM Producto pr 
INNER JOIN Categoria ca ON pr.idCategoria = ca.idCategoria
INNER JOIN Proveedor pro ON pr.idProveedor = pro.idProveedor 
WHERE NombreCategoria='Bebidas' OR NombreCategoria = 'Lacteos';                              

-- uso del objeto
select * from CPP_categoria_producto;

-- VERSIÓN ALTERNA
CREATE VIEW categorias_productos (Ncategoria, Nproducto, Nproveedor) AS
SELECT NombreCategoria, NombreProducto, NombreEmpresa FROM producto prod 
INNER JOIN categoria AS catg ON prod.IdCategoria = catg.IdCategoria
INNER JOIN proveedor AS prov ON prod.IdProveedor = prov.IdProveedor
WHERE CantidadPorUnidad LIKE '%bot%' GROUP BY NombreCategoria;

-- uso del objeto
select * from categorias_productos;



/*4. Cree una vista con el código del cliente, el nombre del cliente y el total 
pagado por cada cliente.poner descuento */
CREATE VIEW CP_ClienTotal AS
SELECT pe.idCliente AS idClien,NombreEmpresa,SUM(PrecioUnidad*Cantidad) AS total_pagado
FROM Pedido pe INNER JOIN Cliente cl ON pe.idCliente = cl.idCliente
INNER JOIN detalles_de_pedido dp ON dp.idPedido = pe.idPedido 
GROUP BY pe.idCliente,NombreEmpresa; 

select * from detalles_de_pedido;

-- uso del objeto
select * from CP_ClienTotal; 

/*5. Cree una vista con el código del cliente, el nombre de cliente y 
el porcentaje que el total pagado por cada cliente representa frente 
al total de todas las ventas registradas.Utilice una subconsulta 
para obtener el total de todas las ventas y la vista del punto 4
para obtener el porcentaje.*/

CREATE VIEW CP_PercentTotal AS
SELECT idClien,NombreEmpresa,-- ROUND es para redondear recibe 2 parametros
ROUND((total_pagado*100)/(SELECT SUM(total_pagado) -- SUBCONSULTA 
FROM CP_ClienTotal),3) AS suma_pagos -- 3 decimales
FROM CP_ClienTotal; 

-- (total_pagado *100) / totalTodos
-- uso del objeto
select * from CP_PercentTotal;

/*6. crear una vista con las todas las columnas de la tabla productos, y solo las filas en las
que el precio unidad es mayor a 20. Esta vista es para actualizar la tabla.
● Insertar en la vista, un registro en el cual incluya como precio unidad 30, tenga en
cuenta la integridad de datos. Describa lo que sucede.
● Cree la sentencia necesaria para modificar, en la vista, el precio unidad a 10 del
producto 77, intente ahora cambiarlo a 28. Describa lo que sucede en los dos casos.*/

CREATE VIEW P_precioProducto AS select * from producto
WHERE PrecioUnidad>20 with check option;  
--  with check option permite o confirma que podamos actualizar la tabla mediante la vista

INSERT P_precioProducto(IdProducto,NombreProducto,IdProveedor,IdCategoria, -- sin el into
CantidadPorUnidad,PrecioUnidad,UnidadesEnExistencia,UnidadesEnPedido,NivelNuevoPedido
,Suspendido)
VALUES(79,'Tofu japones con leche Únicla',2,2,'100 - paq. 50kg',10,12,0,2,0);
SELECT * FROM P_precioProducto;
SELECT * FROM Producto;

UPDATE P_precioProducto SET PrecioUnidad = '21' WHERE IdProducto = 78; -- CHECK OPTION FALLIDO
/*ANTES ESTABA en 30*/
UPDATE P_precioProducto SET PrecioUnidad = '28' WHERE IdProducto = 77; 
/*era un producto menor a 20 no visible pero no lo actualziamos*/
-- no esta visible

/*7. Cree una vista con el requisito Distinct con las regiones de origen de los clientes, omita
las filas con valor nulo. Explique con sus palabras la funcionalidad del requisito Distinct,
en la creación de vistas. */

CREATE VIEW DC_region_cliente AS SELECT DISTINCT Region FROM Cliente WHERE Region IS NOT NULL;  
/*DISTINCT ES PARA SELECCIONAR SOLO LOS VALORES QUE SEAN DIFERENTES si profe con un poco de interferencia
YA QUE HAY MUCHOS DUPLICADOS A VECES*/

-- uso del objeto
select * from DC_region_cliente;


-- PROCEDIMIENTOS ALMACENADOS
-- store procedure
-- SUPER CONSULTA GENERICA PARA AVERIGUAR Y SABER DATOS AL LIMITE SIN DETALLES DE PEDIDO PORQUE SE TOTEA
CREATE PROCEDURE SP_generico() SELECT c.*,e.*,pe.*,p.* FROM pedido pe
INNER JOIN cliente c ON pe.IdCliente = c.IdCliente
INNER JOIN empleado e ON pe.IdEmpleado = e.IdEmpleado
INNER JOIN producto p ON p.IdProducto = p.IdProducto
ORDER BY NombreEmpresa DESC;

CALL SP_generico();


/*1. Crear los SP que permitan insertar datos en las tablas: Empleados, Clientes y Pedidos.*/ 
-- c.*,e.*,p.* 

CREATE VIEW ECP_para_insercion AS 
SELECT c.*,e.*,p.* FROM pedido p
INNER JOIN cliente c ON p.IdCliente = c.IdCliente
INNER JOIN empleado e ON p.IdEmpleado = e.IdEmpleado;

CREATE PROCEDURE SP_i_empleado() 
INSERT ECP_para_insercion(NombreEmpresa)VALUES('Zimbo Bimbo');

CREATE PROCEDURE SP_i_cliente()
INSERT ECP_para_insercion(Nombre)VALUES('Elon');

CREATE PROCEDURE SP_ipedido
(
	cod_c varchar(10), cod_e int(11), fecha_p date, fecha_e date, fecha_envio date,
    cod_et int(11), cargo float, destinatario varchar(80), d_destinatario varchar(120),
	c_destinatario varchar(30), r_destinatario varchar(30), cp_destinatario varchar(20),
    p_destinatario varchar(30)
)
SET @cod_p = (SELECT MAX(IdPedido)+1 FROM pedido);
INSERT ECP_para_insercion(FechaPedido)VALUES(@cod_p,cod_c,cod_e,fecha_p, fecha_e , fecha_envio ,
    cod_et , cargo , destinatario, d_destinatario ,
	c_destinatario , r_destinatario , cp_destinatario,
    p_destinatario );
    
    

-- para eliminar un PA - SP se utiliza drop sin ()
-- USO DEL OBJETO
CALL SP_insercion_aleatoria();




/*2. Crear el SP que permita modificar el precio de un producto, debe recibir el código del producto
y el nuevo precio, tenga en cuenta que el nuevo precio no puede estar por debajo ni por encima
del 5% del precio actual del producto.*/ 

CREATE VIEW VW_PRODUCTO AS SELECT * FROM PRODUCTO WITH CHECK OPTION;

CREATE PROCEDURE SP_ACTUALIZAPRECIO
(
	CODIGOPRODUCTO INT,NUEVOPRECIO FLOAT
)
UPDATE VW_PRODUCTO SET PrecioUnidad = NUEVOPRECIO WHERE NUEVOPRECIO between 
											PrecioUnidad-(PrecioUnidad*5)/100 AND
                                            PrecioUnidad+(PrecioUnidad*5)/100 AND 
                                            idProducto = CODIGOPRODUCTO;
                                            
CALL SP_ACTUALIZAPRECIO(1,18.1);


/*3. Crear un SP que permita suspender o los productos, debe recibir el código del producto, tenga
en cuenta que solo debe permitir suspender el producto cuando las unidades en pedido sean 0.*/ 
-- mejor poner los nombres de bd y despues sp etc
-- profe hasta cuando hay plazo para subir el de procedures-> proximo viernes
CREATE PROCEDURE SP_suspender_producto 
(
	CODIGOPRODUCTO INT 
)
UPDATE VW_PRODUCTO SET Suspendido = 1 WHERE UnidadesEnPedido=0 AND IdProducto = CODIGOPRODUCTO
AND Suspendido = 0;

select * from VW_PRODUCTO;
CALL SP_suspender_producto(1); -- es un update ahh


/*4. El gerente de recursos humanos debe tener disponible la información de cuáles de sus
empleados ya están pensionados, esto quiere decir que ya cumplieron 60 años, en el
procedimiento almacenado, en sql server utilice la siguiente sintaxis para hallar la edad. Para
otro servidor consulte las funciones que realicen los mismos cálculos con fechas.
DATEDIFF(YEAR,NomColumnaTipoFecha,GETDATE())
La función DATEDIFF permite obtener el valor que resulta de extraer una de las partes que
compone un dato de tipo fecha. Se continúa la sintaxis agregando, entre paréntesis y
separando con coma, la parte que se desea obtener (año, mes ó día), se agrega ahora el
nombre de la columna con fecha más antigua y por último, el nombre del campo tipo fecha
más reciente; es importante señalar que la función GETDATE() permite obtener la fecha
actual, fecha del sistema, en la cual se ejecute la sentencia; por lo tanto, es conveniente
usarla para obtener datos referentes a aniversarios como edad, tiempo de servicio, años,
meses o días transcurridos entre dos fechas.
Elabore un procedimiento que le permita al gerente visualizar el nombre completo del
empleado y el tiempo de pensionado que lleva cada uno.*/ 

CREATE VIEW VW_empleados AS select e.*,TIMESTAMPDIFF(YEAR,FechaNacimiento,now())
AS edad,timestampdiff(YEAR,FechaContratación,now()) AS tiempo_servicio FROM
empleado e; -- selección ajustada

CREATE PROCEDURE SP_pensionados() -- uso de la vista con condición
SELECT IdEmpleado,Nombre, Apellidos, edad, edad-60  
AS años_pensionado FROM VW_empleados WHERE edad>60; 

CALL SP_pensionados(); -- para no hacer procedimientos gordos


/*5. El área de recursos humanos debe pasar el reporte anual de cuáles de sus empleados están
próximos a pensionarse, este reporte muestra el nombre completo del empleado, los años
de servicio que lleva y el número de años que falta para pensionarse, El procedimiento debe
mostrar, únicamente, aquellos que tengan entre 55 y 59 años de edad y al menos 17 años
de servicio.*/ 

CREATE procedure SP_proximos_pension()
SELECT IdEmpleado, Nombre, Apellidos, edad, tiempo_servicio, 60 - edad AS años_para_pension 
FROM VW_empleados WHERE edad BETWEEN 55 AND 59 AND tiempo_servicio >= 17;  

CALL SP_proximos_pension(); -- solo el 3 y 6 cumplen


/*6. Elabore un procedimiento almacenado que le permita al área de compras, generar un
reporte diario con los datos de los productos que deben pedir; para ello quieren visualizar
el nombre del producto, precio unitario, el precio promedio en ventas y la diferencia entre
el precio unitario y el precio promedio en ventas, todo esto siempre y cuando el producto
no se encuentre descontinuado (suspendido no debe ser igual a 1).
Además, tenga en cuenta que las unidades en existencia deben ser mayores al valor del nivel
de nuevo pedido(cantidad) o que el nivel nuevo pedido(CANTIDAD) puede ser cero pero las unidades en existencia
son menor o igual al 2.*/

create view VW_ventasProductos as 
select p.*,avg(dp.preciounidad) as media_ventas from producto as p
	  inner join detalles_de_pedido as dp on p.idproducto = dp.idproducto 
      group by p.idproducto; 
-- la vista hace el trabajo duro
-- el procedimiento es carroñero
create procedure SP_infoProductosPeticion()
select idProducto,nombreproducto, preciounidad, media_ventas from VW_ventasProductos 
where suspendido = 0 and UnidadesEnExistencia > NivelNuevoPedido or 
                         NivelNuevoPedido = 0 and UnidadesEnExistencia <= 2; 
        
call SP_infoProductosPeticion(); -- si suspendido es igual a 1 esta suspendido sino esta vigente

select * from producto;



-- if y case en SQL 




-- version alterna no funciona error en el select
/*
CREATE PROCEDURE SP_RC_datos_productos_porpedir()
SELECT dp.IdDetalle,
p.Suspendido AS suspencion,
p.NombreProducto,p.UnidadesEnExistencia,
p.PrecioUnidad,
AVG(SUM(p.PrecioUnidad*dp.Cantidad))AS media_total_ventas,
AVG(SUM(p.PrecioUnidad*dp.Cantidad))-p.PrecioUnidad AS diff_precio_unit_media,
dp.Cantidad 
FROM detalles_de_pedido dp 
INNER JOIN producto p ON p.IdProducto = p.IdProducto
INNER JOIN detalles_de_pedido dp ON dp.IdDetalle = dp.IdDetalle
WHERE p.UnidadesEnExistencia > dp.Cantidad OR
dp.Cantidad = 0 AND 
p.UnidadesEnExistencia <= 2 AND
suspencion==0;
SELECT * FROM detalles_de_pedido;
CALL SP_generico();*/

