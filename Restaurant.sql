-- 24/08/2020 -> 15/09/2020
drop database if exists restaurant;
create database if not exists restaurant;
use restaurant;

create table dinner_table( -- mesa
id int not null auto_increment primary key,
number_table numeric(2,0)
);

create table plate(  -- platos
id int not null auto_increment primary key,
name_plate varchar(60) null,
precio decimal(9,0)
);

create table kitchen( -- cocina o lugar
id int not null auto_increment primary key,
name_customer varchar(50) null,
address       varchar(50) not null,
phone      	  numeric(10,0) not null
);

create table customer( -- cliente
id int not null auto_increment primary key,
name_customer varchar(50) null,
last_name     varchar(50) null,
address       varchar(50) not null,
phone      	  numeric(10,0) not null
);

create table invoice( -- factura
id int not null auto_increment primary key,
customer_id int,
date_invoice date
);

create table waiter( -- mesero
id int not null auto_increment primary key,
name_customer varchar(50) null,
last_name     varchar(50) null,
address       varchar(50) not null,
phone      	  numeric(10,0) not null,
role_waiter   varchar(30) not null,
kitchen_id       int       
);

create table invoice_detail(
id int not null auto_increment primary key,
invoice_id int,
waiter_id int,
plate_id int,
dinner_table_id int	
);

create table booking(
id int not null auto_increment primary key,
customer_id int,
dinner_table_id int,
kitchen_id    int,
date_booking date
);

-- Factura o pedido del cliente
alter table invoice 
add constraint fk_invoice_customer foreign key (customer_id) references customer(id);

-- Detalle de factura
alter table invoice_detail
add constraint fk_invdet_invoice foreign key (invoice_id) references invoice(id);

alter table invoice_detail
add constraint fk_invdet_waiter foreign key (waiter_id) references waiter(id);

alter table invoice_detail
add constraint fk_invdet_plate foreign key (plate_id) references plate(id);

alter table invoice_detail
add constraint fk_invdet_dinner_table foreign key (dinner_table_id) references dinner_table(id);

-- cocina o establecimiento donde labora el mesero
alter table waiter 
add constraint fk_waiter_kitchen foreign key (kitchen_id) references kitchen(id);

-- Reservación
alter table booking 
add constraint fk_booking_customer foreign key (customer_id) references customer(id);

alter table booking 
add constraint fk_booking_dinner_table foreign key (dinner_table_id) references dinner_table(id);

alter table booking 
add constraint fk_booking_kitchen foreign key (kitchen_id) references kitchen(id);












